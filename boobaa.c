#include <stdio.h>
#include <pthread.h>
#include "boobaa.h"

void* ginto(void* arg) {
    int* pn = (int*) arg;
    int n = *pn;
    for (int i = 1; i <= n; i++) {
        if (i % 15 == 0) {
            printf("BooBaa ");
        } else if (i % 3 == 0) {
            printf("Boo ");
        } else if (i % 5 == 0) {
            printf("Baa ");
        } else {
            printf("%d ", i);
        }
    }
    printf("\n");
    pthread_exit(NULL);
}

void ginto_safe(int n) {
    if (n <= 0) {
        fprintf(stderr, "Error: invalid argument\n");
        return;
    }

    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_lock(&mutex);
    for (int i = 1; i <= n; i++) {
        int j = i;
        if (j % 15 == 0) {
            printf("BooBaa ");
        } else if (j % 3 == 0) {
            printf("Boo ");
        } else if (j % 5 == 0) {
            printf("Baa ");
        } else {
            printf("%d ", j);
        }
    }
    printf("\n");
    pthread_mutex_unlock(&mutex);
}